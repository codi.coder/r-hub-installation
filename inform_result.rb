def inform_result(success, failure)
  if $?.exitstatus == 0
    puts "[Success]: #{success}"
  else
    puts "[Fail]: #{failure}"
  end
end

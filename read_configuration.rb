def read_configuration
  {
    hostname: read_config('Hostname', 'r-hub'),
    user: read_config('Your user', 'row'),
    pubkey: read_config("Your user's public key url", 'https://gitlab.com/RowDaBoat/r-hub/raw/master/row.alephia.tech.pub'),
    aws_region: read_config('AWS Region', 'sa-east-1'),
    aws_access_key_id: read_config('AWS Access Key Id'),
    aws_secret_access_key: read_config('AWS Secret Access Key'),
    gitlab_runner_token: read_config('Gitlab runner token') 
  }
end

def read_config(prompt, default=nil)
  print "  · #{prompt} [#{default.nil? ? 'none' : default}]: "
  input = gets.chomp

  input.empty? ? default : input 
end

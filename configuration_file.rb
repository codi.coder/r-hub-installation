class ConfigurationFile
  def self.open(filename)
    ConfigurationFile.new(filename, File.readlines(filename))
  end

  def initialize(filename, lines)
    @filename = filename
    @lines = lines
  end

  def edit(starting_with:)
    @lines = @lines.map { |line| line.start_with?(starting_with) ? "#{yield line.chop}\n" : line }
    self
  end

  def save()
    File.write(@filename, @lines.join())
  end
end

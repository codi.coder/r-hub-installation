require_relative 'inform_result.rb'

def setup_fish(user)
  %x(echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/3/Debian_10/ /' | tee -a /etc/apt/sources.list)
  inform_result('Added fish repo to soruces.list', 'Could not add fish repo to sources.list')

  %x(wget -q -O - https://download.opensuse.org/repositories/shells:fish:release:3/Debian_10/Release.key | apt-key add -)
  inform_result('Added fish repo key', 'Could not add fish repo key')

  %x(apt update && apt install fish)
  inform_result('Fish installed', 'Could not install fish')
  
  change_shell_for('root')
  change_shell_for('pi')
  change_shell_for(user)
end

def change_shell_for(user)
  %x(chsh --shell $(which fish) #{user})
  inform_result("Fish shell set for #{user}", "Could not set fish shell for #{user}")
end

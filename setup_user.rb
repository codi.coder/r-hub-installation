require 'net/http'
require 'uri'
require 'fileutils'
require_relative 'inform_result.rb'

def setup_user(user, pubkey_url)
  create_user(user)
  add_groups(user, pi_groups.append('docker'))
  enable_sudo(user)
  setup_authorized_key(pubkey_url, user)
  puts("")
end

def create_user user
  %x(adduser --disabled-password --gecos "" #{user})
  inform_result("User [#{user}] created.", "Could not create user [#{user}]. Was the user already created?")
end

def add_groups(user, groups)
  %x(usermod -G #{groups.join(',')} #{user})
  inform_result("User [#{user}] added to groups.", "Could not add user [#{user}] to groups.")
end

def pi_groups
  %x(groups pi)
    .split(' ')
    .drop(2)
    .select { |element| element != "pi" }
end

def enable_sudo(user)
  sudoers = "/etc/sudoers.d/#{user}"
  File.open(sudoers, 'w') { |file| file.write("#{user} ALL=(ALL) NOPASSWD: ALL") }
  File.chmod(0440, "#{sudoers}")
  FileUtils.chown('root', 'root', sudoers)
end

def setup_authorized_key(pubkey_url, user)
  pubkey = fetch(pubkey_url)

  if !pubkey.nil?
    authorized_keys = "#{ssh_directory_of(user)}/authorized_keys"
    File.open(authorized_keys, 'a') { |file| file.write(pubkey) }
    puts("[Success]: Added an authorized key to [#{user}] for remote login.")
  else
    puts("[Fail]: Could not add an authorized key to [#{user}] for remote login.")
  end
end

def fetch(url)
  response = Net::HTTP.get_response(URI.parse(url))
  response.code == "200" ? response.body : nil
end

def ssh_directory_of(user)
  ssh_directory = "/home/#{user}/.ssh"
  FileUtils.mkpath(ssh_directory) unless Dir.exist?(ssh_directory)
  ssh_directory
end

def ssh_key_path_of(user)
  "#{ssh_directory_of(user)}/id_rsa"
end

def add_ssh_identity(user)
  %x(echo y | ssh-keygen -t rsa -f #{ssh_key_path_of(user)} -N "")
  inform_result("SSH identity created for #{user}", "Could not create SSH identity for #{user}")
end

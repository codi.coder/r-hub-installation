class Apt
  def initialize()
    @commands = []
  end

  def update()
    @commands.append('apt update')
    self
  end
  
  def install(*packages)
    @commands.append("apt install -y #{packages.join(' ')}")
    self
  end

  def done()
    puts @commands.join('; ')
    %x(#{@commands.join('; ')})
  end
end

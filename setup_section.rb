def setup_section(name)
  banner(name)
  result = yield
  puts ""
  puts ""

  result
end

def banner(label)
  bar = '-' * (label.length + 2)
  puts ".#{bar}."
  puts "| #{label} |"
  puts "'#{bar}'"
end

class KeyValueFile
  def initialize(filename)
    @filename = filename
    @key_values = ''
    @header = ''
  end
  
  def with_header(header)
    @header = header
    self
  end
  
  def add_key_value(key, value)
    @key_values << "#{key}=#{value}\n"
    self
  end
  
  def save()
    File.write(@filename, "#{@header}#{@key_values}")
  end
end

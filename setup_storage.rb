require_relative('apt')

def setup_storage(label)
    Apt.new().update().install('exfat-fuse').done()
    
    storage_path = '/media/storage'
    Dir.mkdir(storage_path) unless Dir.exist?(storage_path)

    return if (label.empty?)

    Fstab.new().add(
      label: label,
      target: storage_path,
      fs: Fstab.auto,
      options: [Fstab.rw, Fstab.auto, Fstab.nouser, Fstab.noexec],
      dump_frequency: 0,
      fscheck_pass_number: 2
    ).done()
end

class Fstab
  def self.rw
    'rw'
  end

  def self.auto
    'auto'
  end

  def self.nouser
    'nouser'
  end

  def self.noexec
    'noexec'
  end

  def initialize
    @entries = []
  end

  def add(label:, target:, fs:, options:, dump_frequency:, fscheck_pass_number:)
    @entries.append("LABEL=#{label}\t#{target}\t#{fs}\t#{options.join(',')}\t#{dump_frequency}\t#{fscheck_pass_number}")
    self
  end

  def done()
    open('/etc/fstab', 'a') { |fstab| fstab << @entries.join("\n") }
  end
end

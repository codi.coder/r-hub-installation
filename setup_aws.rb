require_relative 'key_value_file.rb'

def setup_aws(region, access_key_id, secret_access_key)
  default = "[default]\n"

  if (!Dir.exist?(configuration_directory))
    Dir.mkdir(configuration_directory)
  end

  KeyValueFile.new("#{configuration_directory}/config")
    .with_header(default)
    .add_key_value('region', region)
    .add_key_value('output', 'text')
    .save()

  KeyValueFile.new("#{configuration_directory}/credentials")
    .with_header(default)
    .add_key_value('aws_access_key_id', access_key_id)
    .add_key_value('aws_secret_access_key', secret_access_key)
    .save()

    puts '[Successs] AWS setup'
end

def configuration_directory
  File.expand_path('~/.aws')
end

def setup_hostname(name)
  File.open("/etc/hostname", 'w') { |file| file.puts(name) }
  puts "[Success]: Hostname set up to #{name}"
  puts ""
end

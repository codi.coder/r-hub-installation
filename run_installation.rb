#!/usr/bin/ruby

require_relative 'setup_section.rb'
require_relative 'read_configuration.rb'
require_relative 'setup_hostname.rb'
require_relative 'setup_user.rb'
require_relative 'setup_ssh.rb'
require_relative 'setup_docker.rb'
require_relative 'setup_docker_compose'
require_relative 'setup_aws.rb'
require_relative 'setup_fish.rb'
require_relative 'setup_gitlab_runner.rb'
require_relative 'setup_storage.rb'

puts ""
puts ""

if (ARGV.length == 0)
  configuration = setup_section('Configuration') { read_configuration }
  setup_section('Setup Hostname') { setup_hostname(configuration[:hostname]) }
  setup_section('Setup User') { setup_user(configuration[:user], configuration[:pubkey]) }
  setup_section('Setup SSH') { setup_ssh }
  setup_section('Setup Docker') { setup_docker(configuration[:user]) }
  setup_section('Setup AWS') { setup_aws(configuration[:aws_region], configuration[:aws_access_key_id], configuration[:aws_secret_access_key]) }
  setup_section('Setup Fish') { setup_fish(configuration[:user]) }
  setup_section('Setup Gitlab Runner') { setup_gitlab_runner(configuration[:gitlab_runner_token], configuration[:hostname]) }
  setup_section('Setup Storage') { setup_storage(configuration[:storage_label])}
else
  case ARGV[0]
    when 'hostname'
      setup_section('Setup Hostname') { setup_hostname(ARGV[1]) }
    when 'user'
      setup_section('Setup User') { setup_user(ARGV[1], ARGV[2]) }
    when 'ssh'
      setup_section('Setup SSH') { setup_ssh(ARGV[1]) }
    when 'docker'
      setup_section('Setup Docker') { setup_docker(ARGV[1]) }
    when 'aws'
      setup_section('Setup AWS') { setup_aws(ARGV[1], ARGV[2], ARGV[3]) }
    when 'fish'
      setup_section('Setup Fish') { setup_fish(ARGV[1]) }
    when 'install_gitlab_runner'
      setup_section('Install Gitlab Runner') { install_gitlab_runner }
    when 'setup_gitlab_runner'
      setup_section('Setup Gitlab Runner') { setup_gitlab_runner(ARGV[1], %x(uname -n).gsub(/\n/, '')) }
    when 'setup_storage'
      setup_section('Setup Storage') { setup_storage(ARGV[1]) }
    else
      "Error: invalid setup choice"
  end
end

puts "R Hub installed!"

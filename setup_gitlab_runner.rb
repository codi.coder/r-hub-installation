require 'fileutils'
require 'securerandom'
require_relative 'inform_result.rb'
require_relative 'setup_user.rb'

def install_gitlab_runner
  %x(curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash)
  inform_result('Gitlab Runner install script downloaded', 'Could not download Gitlab Runner install script')

  %x(apt-get install -y gitlab-runner)
  inform_result('Gitlab Runner installed', 'Could not install Gitlab Runner')

  configure_gitlab_runner
end

def setup_gitlab_runner(token, host)
  %x(gitlab-ci-multi-runner register \
    --non-interactive \
    --name #{host} \
    --url "https://gitlab.com" \
    --registration-token #{token} \
    --executor "docker" \
    --docker-image "ruby:2.6" \
    --docker-privileged=true \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  )
  inform_result('Gitlab Runner registered', 'Could not register Gitlab Runner')
end

def configure_gitlab_runner
  begin
    runner_config = '/etc/gitlab-runner/config.toml'
    FileUtils.cp('config.toml', runner_config)
    FileUtils.chmod('u=rw,go=', runner_config)
  rescue
    puts "[Fail]: Could not configure Gitlab Runner"
    return
  end
  
  puts '[Success]: Gitlab Runner configured'
end
